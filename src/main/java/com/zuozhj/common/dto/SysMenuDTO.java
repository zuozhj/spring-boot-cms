package com.zuozhj.common.dto;

import java.util.ArrayList;
import java.util.List;

import java.io.Serializable;

import lombok.Data;

@Data
public class SysMenuDTO implements Serializable {
    
    private Long id;

    private String name;

    private String title;

    private String icon;

    private String path;

    private String component;

    private List<SysMenuDTO> children = new ArrayList<>();

}
