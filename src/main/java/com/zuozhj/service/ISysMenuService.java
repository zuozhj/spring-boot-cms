package com.zuozhj.service;

import com.zuozhj.common.dto.SysMenuDTO;
import com.zuozhj.entity.SysMenu;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zuozhj
 * @since 2022-12-02
 */
public interface ISysMenuService extends IService<SysMenu> {

    List<SysMenuDTO> getCurrentUserNav();

    List<SysMenu> tree();

}
