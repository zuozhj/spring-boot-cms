package com.zuozhj.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

public interface UploadFileService {
    
    String upload(MultipartFile uploadFile) throws IOException;
}
