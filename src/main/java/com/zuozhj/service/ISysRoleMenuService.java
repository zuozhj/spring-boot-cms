package com.zuozhj.service;

import com.zuozhj.entity.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zuozhj
 * @since 2022-12-02
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

}
