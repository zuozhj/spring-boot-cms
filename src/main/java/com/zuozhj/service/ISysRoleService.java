package com.zuozhj.service;

import com.zuozhj.entity.SysRole;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zuozhj
 * @since 2022-12-02
 */
public interface ISysRoleService extends IService<SysRole> {

    List<Long> getMenuByRoleId(Long id);

    List<SysRole> listRolesByUserId(Long id);

}
