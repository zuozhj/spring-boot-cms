package com.zuozhj.service.impl;

import com.zuozhj.entity.SysUserRole;
import com.zuozhj.mapper.SysUserRoleMapper;
import com.zuozhj.service.ISysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zuozhj
 * @since 2022-12-02
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

}
