package com.zuozhj.service.impl;

import com.zuozhj.entity.SysRoleMenu;
import com.zuozhj.mapper.SysRoleMenuMapper;
import com.zuozhj.service.ISysRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zuozhj
 * @since 2022-12-02
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements ISysRoleMenuService {

}
