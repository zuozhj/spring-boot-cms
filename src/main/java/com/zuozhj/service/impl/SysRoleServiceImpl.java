package com.zuozhj.service.impl;

import com.zuozhj.entity.SysRole;
import com.zuozhj.entity.SysRoleMenu;
import com.zuozhj.mapper.SysRoleMapper;
import com.zuozhj.service.ISysRoleMenuService;
import com.zuozhj.service.ISysRoleService;
import com.zuozhj.service.ISysUserRoleService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zuozhj
 * @since 2022-12-02
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

    @Autowired
    ISysRoleMenuService sysRoleMenuService;

    @Autowired
    ISysUserRoleService sysUserRoleService;


    @Override
    public List<Long> getMenuByRoleId(Long id) {
        
        List<SysRoleMenu> roleMenus = sysRoleMenuService.list(new QueryWrapper<SysRoleMenu>().eq("role_id", id));

        List<Long> menuIds = roleMenus.stream().map(rm -> rm.getMenuId()).collect(Collectors.toList());

        return menuIds;
    }

    @Override
    public List<SysRole> listRolesByUserId(Long userId) {

        List<SysRole> roles = this.list(new QueryWrapper<SysRole>().inSql("id", "select role_id from sys_user_role where user_id = " + userId));

        return roles;
    }

}
