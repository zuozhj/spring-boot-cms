package com.zuozhj.utils;

import java.util.Date;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Data;

@Data
@Component
@ConfigurationProperties(prefix = "jwt")
public class JwtUtils {
    
    private long expire;
    private String secret;
    private String header;

    public String generateToken(String username) {

        Date nowDate = new Date();
        Date expireDate = new Date(nowDate.getTime() + 1000 * expire);

        return Jwts.builder()
                .setHeaderParam("type", "JWT")
                .setSubject(username)
                .setIssuedAt(nowDate) // 生成时间
                .setExpiration(expireDate) // 过期时间
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }


    public Claims getClaimByToken(String jwt) {

        try{
            return Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(jwt)
                    .getBody();
        } catch (Exception e) {
            return null;
        }
    }


    public boolean isTokenExpired(Claims claims) {
        return claims.getExpiration().before(new Date());
    }
}
