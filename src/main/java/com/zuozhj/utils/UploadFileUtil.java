package com.zuozhj.utils;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.zuozhj.service.UploadFileService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UploadFileUtil implements UploadFileService {
    
    @Override
    public String upload(MultipartFile uploadFile) throws IOException {

         //获取文件原始名
         String originalFilename = uploadFile.getOriginalFilename();

         //构造唯一的文件名  uuid（通用唯一识别码--长度固定的字符串 de49685b-6lc0-4b11-80fa-c7le95924018）
         int index = originalFilename.lastIndexOf(".");
         String extName = originalFilename.substring(index);//获取后缀文件名
         String newFileName= UUID.randomUUID().toString() + extName;
         log.info("获取到的文件名：{}",newFileName);
        System.out.println("---------------------------------------------------");
        String path = "F:\\upload\\" + newFileName;
         //将文件存入指定的目录
         uploadFile.transferTo(new File("F:\\upload\\" ,newFileName));
        return path;
    };

}
