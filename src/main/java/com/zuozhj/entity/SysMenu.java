package com.zuozhj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zuozhj
 * @since 2022-12-02
 */
@Data
@EqualsAndHashCode(callSuper = false)

public class SysMenu implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @NotNull(message = "上级菜单不能为空")
    private Long parentId;

    @NotBlank(message = "菜单名称不能为空")
    private String name;

    private String path;

    @NotBlank(message = "菜单编码不能为空")
    private String perms;

    private String component;

    @NotNull(message = "菜单类型不能为空")
    private Integer type;

    private String icon;

    @TableField("orderNum")
    private Integer orderNum;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private Integer status;

    // 自定义字段,数据库不存在该字段
    @TableField(exist = false)
    private List<SysMenu> children = new ArrayList<>();

}
