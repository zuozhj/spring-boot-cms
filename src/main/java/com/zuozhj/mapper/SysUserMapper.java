package com.zuozhj.mapper;

import com.zuozhj.entity.SysUser;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zuozhj
 * @since 2022-12-02
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    List<Long> getNavMenuIds(Long userId);



;


}
