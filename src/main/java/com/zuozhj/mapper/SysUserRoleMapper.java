package com.zuozhj.mapper;

import com.zuozhj.entity.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zuozhj
 * @since 2022-12-02
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
