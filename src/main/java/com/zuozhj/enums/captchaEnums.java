package com.zuozhj.enums;

import lombok.Getter;

@Getter
public enum captchaEnums {
    
    YZM(1, "验证码")
    ;

    private Integer code;

    private String message;

    captchaEnums(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
