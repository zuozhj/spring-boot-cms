package com.zuozhj.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.ServletRequestUtils;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zuozhj.service.ISysMenuService;
import com.zuozhj.service.ISysRoleService;
import com.zuozhj.service.ISysUserService;
import com.zuozhj.utils.RedisUtil;

public class BaseController {
    @Autowired
    HttpServletRequest request;

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    ISysUserService sysUserService;

    @Autowired
    ISysRoleService sysRoleService;

    @Autowired
    ISysMenuService sysMenuService;

    // 分页
    public Page getPage() {

        int current  = ServletRequestUtils.getIntParameter(request, "current", 1);
        int size  = ServletRequestUtils.getIntParameter(request, "size", 20);

        return new Page(current, size);
    }
}
