package com.zuozhj.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zuozhj
 * @since 2022-12-04
 */
@RestController
@RequestMapping("/users")
public class UsersController {

}
