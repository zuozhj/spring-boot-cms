package com.zuozhj.controller;

import java.util.UUID;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.code.kaptcha.Producer;
import com.zuozhj.common.Result;
import com.zuozhj.utils.EmailUtil;
import com.zuozhj.utils.UploadFileUtil;

import cn.hutool.core.codec.Base64Encoder;
import cn.hutool.core.map.MapUtil;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.ResourceUtils;

@RestController
public class TestController extends BaseController {

    @Autowired
    Producer producer;

    @Autowired
    EmailUtil emailUtil;

    @Autowired
    UploadFileUtil uploadFileUtil;
    
    // @PreAuthorize("hasRole('admin')")
    // @PreAuthorize("hasAuthority('sys:user:list')") 判断用户角色和用户权限的写法区别
    @RequestMapping("/test")
    public void hello(MultipartFile file) throws FileNotFoundException {
        // 32位随机码
        /* String key = UUID.randomUUID().toString();

        String code = producer.createText();

        BufferedImage image = producer.createImage(code);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", outputStream);

        Base64Encoder encoder = new Base64Encoder();
        String str = "data:image/jpeg;base64,";

        String base64Img = str + encoder.encode(outputStream.toByteArray());

        redisUtil.hset("captcha",key, code, 120);

        return Result.success(
            MapUtil.builder()
                .put("key", key)
                .put("captchaImg", base64Img)
                .build()
        ); */
        // BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        // String pwd = encoder.encode("111111");
        System.out.println(file);
        // try {
        //     // emailUtil.sendHtmlMail("506591429@qq.com", "测试", "springboot 邮件测试");
        //     String path = uploadFileUtil.upload(file);
        //     System.out.println(path);
        // } catch (Exception e) {
        //     // TODO: handle exception
        //     System.out.println("---------------------------------------------------");
        //     System.out.println(e);
        // }
        String path = ResourceUtils.getURL("classpath:").getPath();
        System.out.println(path);
    }

    @RequestMapping("/aaa")
    public Result captcha() throws IOException  {
        String key = UUID.randomUUID().toString();
        String code = producer.createText();

        key = "aaaaa";
        code = "111111";

        BufferedImage image = producer.createImage(code);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", outputStream);

        Base64Encoder encoder = new Base64Encoder();
        String str = "data:image/jpeg;base64,";

        String base64Img = str + encoder.encode(outputStream.toByteArray());

        redisUtil.hset("captcha", key, code, 120);

        return Result.success(
            MapUtil.builder()
                .put("key", key)
                .put("code", code)
                .put("captchaImg", base64Img)
                .build()
        );
    }
}
