1. 接口返回封装数据, 数据格式
    {
        "code": 200,
        "msg": "操作成功",
        "data": {}
    }


2. 验证码接口 /captcha
    返回的的base64位数据流

3. 权限菜单接口 /sys/menu/nav
    返回树状结构数据 ,如下:

		{
			"id": 1,
			"name": "sys:manage",
			"title": "系统管理",
			"icon": "el-icon-s-operation",
			"path": null,
			"component": null,
			"children": [
				{
					"id": 2,
					"name": "sys:user:list",
					"title": "用户管理",
					"icon": null,
					"path": "/sys/users",
					"component": "sys/User",
					"children": [
						{
							"id": 8,
							"name": "sys:user:save",
							"title": "添加用户",
							"icon": null,
							"path": null,
							"component": null,
							"children": []
						},
						{
							"id": 9,
							"name": "sys:user:update",
							"title": "修改用户",
							"icon": null,
							"path": null,
							"component": null,
							"children": []
						}
					]
				},
			  ]
		}  

4. 获取当前用户信息接口 /sys/userInfo
	返回数据 id, username, avatar
	
    
5. 菜单权限的crud  /sys/menu/info/{id}
                   /sys/menu/list
                   /sys/menu/save
                   /sys/menu/update
                   /sys/menu/delete/{id}
        menu 表字段, created_at, update_at
        "parentId": 2,
        "name": "test111",
        "path": null,
        "perms": "sys:user:test11",
        "component": null,
        "type": 2,
        "icon": null,
        "orderNum": 5,
        "status" : 1

6. 角色的crud  /sys/role/info/{id}
               /sys/role/list
               /sys/role/save
               /sys/role/update
               /sys/role/delete/{id}   
               /sys/role/perm/{id}   // 角色的权限设置

        role表字段 name, code, remark, status
        
7. 用户的crud /sys/user/info/{id}
               /sys/user/list
               /sys/user/save
               /sys/user/update
               /sys/user/delete/{id}  
               /sys/user/role  // 分配用户角色
               /sys/user/repass // 初始化密码
	
8. 修改当前用户的密码接口 /sys/updatePassword
	需要字段： password  currentPassword

